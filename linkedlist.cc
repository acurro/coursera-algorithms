
#include "linkedlist.h"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <assert.h>
#include <list>

//================================================= iterator

template<typename Type>
class __ll_iterator : public std::iterator<std::input_iterator_tag,
                                        Type,
                                        Type,
                                        const Type*,
                                        Type>
{
private:
    Node<Type>* _cur;

public:
    typedef Type& reference;
    typedef Type* pointer;
    typedef __ll_iterator<Type> Self;

    __ll_iterator(): _cur() {}

    explicit __ll_iterator(Node<Type>* node): _cur(node) {}

    Self& operator++()
    {
        _cur = _cur->next;
        return *this;
    }
    Self operator++(int)
    {
        Self tmp = *this;
        _cur = _cur->next;
        return tmp;
    }

    bool operator==(const Self& other) const { return other->_cur == _cur; }
    bool operator!=(const Self& other) const { return other->_cur != _cur; }
    reference operator*() const { return _cur->data; }
    pointer operator->() const { return std::addressof(_cur->_data); }

};

template<typename Type>
LinkedList<Type>::iterator LinkedList<Type>::begin() {
    return iterator(this->_root);
}

template<typename Type>
LinkedList<Type>::iterator LinkedList<Type>::end() {
    return iterator(this->_tail);
}


//================================================= LinkedList

template<typename Type>
LinkedList<Type>::LinkedList()
{
    this->_size = 0;
    this->_root = nullptr;
    this->_tail = nullptr;
}

template<typename Type>
LinkedList<Type>::LinkedList(const LinkedList<Type> &other)
{
    Type val = other.root();
}

template<typename Type>
LinkedList<Type>::~LinkedList()
{
    Node<Type>* it = this->_root;
    while(it != this->_tail) {
        it = this->_root->next;
        delete this->_root;
        this->_root = it;
    }
    delete this->_tail;
}

template<typename Type>
Type& LinkedList<Type>::operator++()
{
    if ( _cur->next ) {
        Node<Type>* next(_cur->next());
        _cur = next;
    }
    return _cur;
}

template<typename Type>
void LinkedList<Type>::insert(const Type& val)
{
    Node<Type>* node(new Node<Type>());
    node->data = val;
    node->next = nullptr;
    if (this->_tail) {
        std::cout << "Tail found with " << this->_tail->data << ", point next to node" << std::endl;
        this->_tail->next = node;
        this->_tail = node;
    }
    else {
        if (!this->_root) {
            std::cout << "No root, pointing root to node" << std::endl;
            this->_root = node;
        }
        if (!this->_tail) {
            std::cout << "No tail, pointing tail to root" << std::endl;
            this->_tail = this->_root;
        }
    }
    ++this->_size;
}

template<typename Type>
void LinkedList<Type>::remove(const std::uint16_t index)
{
    // TODO: throw an exception if index is out of scope
    //if ()
//    if (this->_tail) {
//        this->_tail;
//    }
    --this->_size;
}

template<typename Type>
const Type& LinkedList<Type>::root()
{
    return this->_root->data;
}

/// Returns the tail of the list
template<typename Type>
const Type& LinkedList<Type>::tail()
{
    return this->_tail->data;
}

/// Looks for an item stored in the list.
template<typename Type>
const std::uint16_t LinkedList<Type>::find(const Type& value)
{
    return this->_tail->data;
}

/// Returns the number of nodes available in the list
template<typename Type>
const std::uint16_t LinkedList<Type>::size() {
    return _size;
}

template<typename Type>
void LinkedList<Type>::show() {
    Node<Type>* it = this->_root;
    while (it != this->_tail) {
        std::cout << it->data << " -> ";
        it = it->next;
    }
    std::cout << it->data << std::endl;
}



int main(void) {
    int a(10);
    int b(20);
    int c(30);

    LinkedList<int> lista;
    lista.insert(a);
    lista.insert(b);
    lista.insert(c);
    assert(lista.size() == 3);

    lista.show();
}
